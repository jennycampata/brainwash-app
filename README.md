# FA_ReactApp

Introduction: Users can create a prfoile and add personal experience on the app. Request to access the personal profiles of users with the help of push notifications.


Requirements: It requires the following modules.

Nodejs and NPM is required to create a project

We used react-native-router-flux for seamless navigation

React-native-vector-icons for making icons

Leverage redux for globally state management 

Redux-saga for asynchronous API calls in redux action

The app has no menu or further configuration

Configuration: Configured the modules which include React Native vector icons and library.

The documentation is on the Github library.

