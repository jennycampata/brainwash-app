import { Provider } from "react-redux";
import configureStore from "./store";

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";

import { Route } from "./navigator";
import AppNavigator from "./navigator";
// import SplashScreen from "react-native-splash-screen";
export default class App extends Component {
  //   componentDidMount() {
  //     // do stuff while splash screen is shown
  //     // After having done stuff (such as async tasks) hide the splash screen
  //     setTimeout(() => {
  //       SplashScreen.hide();
  //     }, 2000);
  //   }
  render() {
    const store = configureStore();
    return (
      <Provider store={store}>
        <AppNavigator />
      </Provider>
    );
  }
}
