// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,

  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet,
  ImageBackground,
  Alert,
} from "react-native";
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import DatePicker from "react-native-datepicker";
import styles from "./styles";
import Icon from "react-native-vector-icons/FontAwesome";
import { Images, Metrics, Colors, Fonts } from "../../theme";

class RegistrationwasherScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: false,
      name: '',
      email: '',
      password: '',
      confirmpassword: '',
      Dob: null,
      role: 'washer',
      gender: '',
      mobile: null,
      university: '',
      error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false},
    };
  }
  onChangeFirstName = (e) => {
    this.setState({ name: e });
  };
  onChangeNumber = (e) => {
    this.setState({ mobile: e });
  };
  onChangeEmail = (e) => {
    this.setState({email:e});
   };
  onChangePassword = (e) => { 
    this.setState({password:e});
  };

  onChangeConfirmPassword = (e) => {
    this.setState({confirmpassword:e});
   };
  onchangeDob = (e) => {
    this.setState({Dob:e});
   };
  onchangeRole = (e) => {
    this.setState({role:e});
   }
  onchangeGender = (e) => {
    this.setState({gender:e});
   }

  onchangeUniversity = (e) => { 
    this.setState({university:e})
  };

  onSubmitHandle = ()=>{
    const {name,email,password,confirmpassword,university,role,gender,Dob,mobile} = this.state;
    if(name==""|| name==" " || name.length < 3){
      this.setState({
        error:{nameErr:true,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    }
    else if(!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) || email == "" || email == " "){
      this.setState({
        error:{nameErr:false,emailErr:true,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    }
    else if(!mobile || mobile.length < 7){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:true,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    } 
    else if(!Dob){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:true,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    } 
    else if(password.length < 6){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:true,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    }
    else if(confirmpassword !== password){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:true,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    }
    else if(university == "" || university == " "){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:true}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    } 
    else if(gender == ""){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:true,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    } 
    else if(gender == ""){
      this.setState({
        error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:true,genderErr:false,mobileErr:false,unversityErr:false}
      });
      setTimeout(() => {
        this.setState({
          error:{nameErr:false,emailErr:false,passwordErr:false,cPasswordErr:false,dobErr:false,roleErr:false,genderErr:false,mobileErr:false,unversityErr:false}
        });
      }, 3000);
    }
    else{
      Alert.alert('SUCCESSFUL','You have been Registered successfully', [
        
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]);
      this.props.navigation.navigate.pop();
    }
  };
  renderStartDatepicker = () => {
  
    return (
      <DatePicker
        style={{ width: Metrics.ratio(200) }}
        date={this.state.startDate}
        mode="date"
        placeholder="YYYY-MM-DD"
        format="YYYY-MM-DD"
        // minDate="2016-05-01"
        // maxDate={"2019-05-01"}
        confirmBtnText="Confirm"
        cancelBtnText="Cancel"
        style={{
          width: Metrics.screenWidth * 0.9,
          borderBottomWidth: StyleSheet.hairlineWidth,
          borderBottomColor: "#b4b4b4"
        }}
        customStyles={{
          dateIcon: {
            // marginLeft: Metrics.ratio(-40),
            // marginBottom: Metrics.ratio(10),
            //top: Metrics.ratio(10),
            elevation: 8
          },
          dateInput: { borderColor: "transparent" },
          placeholderText: {
            fontSize: Metrics.ratio(14),
            fontFamily: Fonts.type.regular,
            // textAlign:'right',
            color: "#b4b4b4",
            marginRight: Metrics.ratio(10),
            // backgroundColor: "green"
          }
        }}
        onDateChange={date => {
          this.onchangeDob(date);
        }}
      />
    );
  };
  onClickEyeIcon = () => {
    if (this.state.showpassword === true)
      {
        this.setState({showpassword:false});
      }

    else if (this.state.showpassword === false)
    {
      this.setState({showpassword:true});
    }

  };
  renderInputfield = (headerText, placeholder,ErrTxt,Iserr, onChangeText, image,rightIcon, onRightIconClick) => {
   
    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <Image
            source={image}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginTop: Metrics.ratio(6)
            }}
          />
          {/* <Icon style={{}} size={25} color="#0f5997" name={"user"} /> */}
          <TextInput
            style={styles.inputField}
            placeholderTextColor="#b4b4b4"
            secureTextEntry={rightIcon ? this.state.showpassword : false}
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
          {/* {rightIcon && <TouchableOpacity  style={{position:"absolute",right:Metrics.ratio(0)}} onPress={() => onRightIconClick()}>
            <Image source={this.state.showpassword ? Images.view : Images.hide} />
          </TouchableOpacity>} */}
        </View>
        {Iserr && <View><Text style={{color:'red'}} >**{ErrTxt}</Text></View>}
           
      </View>
    );
  };
  renderRadio = (headerText, placeholder1, placeholder2 ,ErrTxt,Iserr, onChangeRadio) => {
    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            // borderBottomWidth: StyleSheet.hairlineWidth,
            // borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <RadioGroup
            color="#ff7ee7"
            style={styles.radioGroup}
            onSelect={(index, value) => {
              onChangeRadio(value)
            }}
          >
            <RadioButton value={placeholder1} color="#b4b4b4" style={styles.radioOptions} >
              <Text>{placeholder1}</Text>
            </RadioButton>


            <RadioButton value={placeholder2} style={styles.radioOptions}>
              <Text>{placeholder2}</Text>
            </RadioButton>
          </RadioGroup>
        </View>
        {Iserr && <View><Text style={{color:'red'}} >**{ErrTxt}</Text></View>}
      </View>
    );
  };
  render() {
    return (
      <ImageBackground
        source={Images.loginBackground}
        // resizeMode = 'contain'
        resizeMethod='auto'
        style={{ width: '100%', height: '100%' }}
      >
        {/* <View style={styles.container}> */}

        {/*  */}
        <ScrollView keyboardShouldPersistTaps="always">

          <View style={styles.bodyView}>
            <View style={styles.logoheaderView}>
              <Image
                source={Images.logo}
                style={{
                  resizeMode: 'center',
                  resizeMethod: 'auto',
                  width: Metrics.ratio(200),
                  height: Metrics.ratio(200),
                  marginBottom: Metrics.ratio(15)
                }}
              />
            </View>
            <View style={styles.headerView}>

              <Text style={styles.headerText}>SIGN UP</Text>
            </View>
            {this.renderInputfield(
              "FULL NAME",
              "Enter Name",
              "Name Required must be content atleast 4 words",
              this.state.error.nameErr,
              this.onChangeFirstName,
              Images.firstNameIcon
            )}
            {this.renderInputfield(
              "EMAIL",
              "Enter Email",
              "email Required should be someone@example.edu",
              this.state.error.emailErr,
              this.onChangeEmail,
              Images.emailIcon
            )}
            {this.renderInputfield(
              "MOBILE NUMBER",
              "Enter Mobile Number",
              "mobile No Required",
              this.state.error.mobileErr,
              this.onChangeNumber,
              Images.mobileNumber
            )}
            <View style={{ flexDirection: "row" }}>
              <View style={{ marginLeft: Metrics.ratio(10), marginBottom: Metrics.ratio(5) }}>
                <Text
                  style={{
                    color: "black",
                    fontSize: Metrics.ratio(14),
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  DATE OF BIRTH
              </Text>
                {this.renderStartDatepicker()}
                {this.state.error.dobErr && <View><Text style={{color:'red'}} >**Date of birth Required</Text></View>}
              </View>
            </View>

            {/* {this.renderInputfield(
              "DATE OF BIRTH",
              "DD/MM/YYYY",
              "date",
              this.onChangeNumber,
              Images.calender
            )} */}

            {this.renderInputfield(
              "PASSWORD",
              "Enter Password",
              "password contain atleast 6 characters",
              this.state.error.passwordErr,
              this.onChangePassword,
              Images.passwordIcon,
              Images.passwordIcon,
              this.onClickEyeIcon
            )}
            {this.renderInputfield(
              "CONFIRM PASSWORD",
              "Enter Confirm Password",
              "password not matched",
              this.state.error.cpasswordErr,
              this.onChangeConfirmPassword,
              Images.passwordIcon,
              Images.passwordIcon,
              this.onClickEyeIcon
            )}
            {this.renderInputfield(
              "UNIVERSITY ATTENDING",
              "Enter University Attending",
              "Enter University Name",
              this.state.error.unversityErr,
              this.onchangeUniversity,
              Images.Scholar
            )}
            {this.renderRadio(
              "GENDER",
              "MALE",
              "FEMALE",
              "Select Gender",
              this.state.error.genderErr,
              this.onchangeGender
            )}
            {/* {this.renderRadio(
              "LOGIN AS",
              "USER",
              "WASHER",
              "Select Role",
              this.state.error.roleErr,
              this.onchangeRole
            )} */}
            <TouchableOpacity style={styles.submitButtonView} onPress={()=>this.onSubmitHandle()}>

              <Text
                style={{
                  color: "black",
                  fontSize: Metrics.ratio(14),
                  fontFamily: Fonts.type.demibold
                }}
              >
                SUBMIT
              </Text>
            </TouchableOpacity>
            <View
              style={{
                flexDirection: "row",
                width: Metrics.screenWidth * 0.9,
                justifyContent: "flex-end",
                marginTop: Metrics.ratio(10)
              }}
            >
              <Text
                style={{
                  fontSize: Metrics.ratio(13),
                  color: "black",
                  fontFamily: Fonts.type.demibold
                }}
              >
                ALREADY ACCOUNT ?{" "}
              </Text>
              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("loginScreen", {
                    screen: "loginScreen"
                  });
                }}
              >
                <Text
                  style={{
                    fontSize: Metrics.ratio(13),
                    color: "#0f5997",
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  LOGIN{" "}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {/* </View> */}
      </ImageBackground>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(RegistrationwasherScreen);
