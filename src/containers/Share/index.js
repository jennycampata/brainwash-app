// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  ScrollView,
  TouchableOpacity
} from "react-native";
import styles from "./styles";
import { Metrics, Fonts, Images } from "../../theme";
import QRCode from "react-native-qrcode";

class ShareScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: "http://facebook.github.io/react-native/"
    };
  }

  renderInputfield = (headerText, placeholder, onChangeText, image) => {
    return (
      <View
        style={[
          styles.inputFieldView,
          headerText === "PHONE NUMBER" && { marginTop: Metrics.ratio(0) }
        ]}
      >
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <Image
            source={image}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginTop: Metrics.ratio(6)
            }}
          />
          {/* <Icon style={{}} size={25} color="#0f5997" name={"user"} /> */}
          <TextInput
            style={styles.inputField}
            placeholderTextColor="#b4b4b4"
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.headerView}>
          <View style={styles.headerTextView}>
            <Text style={styles.headerText}>SHARE</Text>
          </View>
        </View>
        <ScrollView>
          <View
            style={{
              position: "absolute",
              marginTop: Metrics.ratio(60),
              marginHorizontal: Metrics.ratio(130)
            }}
          >
            <QRCode
              value={this.state.text}
              size={100}
              bgColor="black"
              fgColor="white"
            />
          </View>

          <Text
            style={{
              width: Metrics.screenWidth,
              color: "black",
              fontSize: Metrics.ratio(17),
              fontFamily: Fonts.type.demibold,
              textAlign: "center",
              paddingVertical: Metrics.ratio(20)
            }}
          >
            Jenny, Share your QR Code
          </Text>
          <Text
            style={{
              textAlign: "center",
              marginTop: Metrics.ratio(110),
              fontSize: Metrics.ratio(14),
              color: "black",
              fontFamily: Fonts.type.regular
            }}
          >
            or send to an individual
          </Text>
          <View
            style={{
              width: Metrics.screenWidth * 0.95,
              marginHorizontal: Metrics.screenWidth * 0.025,
              marginTop: Metrics.screenHeight * 0.015,
              borderRadius: Metrics.ratio(10),
              backgroundColor: "#ffffff",
              paddingVertical: Metrics.ratio(20),
              elevation: 8,
              marginBottom: Metrics.screenHeight * 0.15
            }}
          >
            {this.renderInputfield(
              "EMAIL ADDRESS",
              "Enter Email Address",
              this.onChangeEmail,
              Images.emailIcon
            )}

            <View
              style={{
                flexDirection: "row",
                width: Metrics.screenWidth,
                alignItems: "center",
                justifyContent: "center",
                marginVertical: Metrics.ratio(15)
              }}
            >
              <View
                style={{
                  width: Metrics.screenWidth * 0.15,
                  height: StyleSheet.hairlineWidth,
                  backgroundColor: "#b4b4b4",
                  marginRight: Metrics.ratio(2)
                }}
              ></View>
              <View
                style={{
                  width: Metrics.ratio(30),
                  height: Metrics.ratio(30),
                  backgroundColor: "#b4b4b4",
                  borderRadius: 100,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Text
                  style={{
                    fontSize: Metrics.ratio(10),
                    color: "black",
                    fontFamily: Fonts.type.regular
                  }}
                >
                  OR
                </Text>
              </View>
              <View
                style={{
                  width: Metrics.screenWidth * 0.15,
                  height: StyleSheet.hairlineWidth,
                  backgroundColor: "#b4b4b4",
                  marginLeft: Metrics.ratio(2)
                }}
              ></View>
            </View>
            {this.renderInputfield(
              "PHONE NUMBER",
              "Enter Phone Number",
              this.onChangeEmail,
              Images.phoneIcon
            )}

            <TouchableOpacity
              onPress={() => {
                this.handleLogin();
              }}
              style={styles.sendButtonView}
            >
              <Text
                style={{
                  color: "white",
                  fontSize: Metrics.ratio(16),
                  fontFamily: Fonts.type.demibold
                }}
              >
                SEND
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(ShareScreen);
