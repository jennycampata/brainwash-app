// @flow
import { StyleSheet } from "react-native";
import { Colors, Metrics, Fonts } from "../../theme";

export default StyleSheet.create({
  container: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight,
    backgroundColor: "#f3f5f6" //#0f5997
  },
  headerView: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight * 0.1,
    flexDirection: "row",
    backgroundColor: "#0f5997"
  },
  headerTextView: {
    width: Metrics.screenWidth,
    height: Metrics.screenHeight * 0.1,
    justifyContent: "center",
    alignItems: "center"
  },
  headerText: {
    color: "white",
    fontSize: Metrics.ratio(17),
    // marginLeft: Metrics.ratio(140),
    fontFamily: "AvenirNext-DemiBold"
  },
  inputFieldView: {
    width: Metrics.screenWidth * 0.9,
    marginHorizontal: Metrics.screenWidth * 0.025
  },
  inputFieldHeaderText: {
    color: "black",
    fontSize: Metrics.ratio(14),
    fontFamily: Fonts.type.demibold
  },
  inputField: {
    width: Metrics.screenWidth * 0.8,
    paddingLeft: Metrics.ratio(10),
    paddingBottom: Metrics.ratio(-10),
    fontSize: Metrics.ratio(14),
    fontFamily: Fonts.type.regular,
    color: "#b4b4b4"
  },
  sendButtonView: {
    width: Metrics.screenWidth * 0.9,
    height: Metrics.ratio(45),
    marginLeft: Metrics.screenWidth * 0.025,
    marginTop: Metrics.ratio(20),
    backgroundColor: "#0f5997",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: Metrics.ratio(30),
    flexDirection: "row",
    elevation: 4
  }
});
