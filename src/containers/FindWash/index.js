// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import { Text, View, StyleSheet, TextInput, Image, TouchableOpacity, BackHandler, ScrollView } from "react-native";
import styles from "./styles";
import { Header } from "../../components";
import { Fonts, Metrics, Images } from "../../theme";
import DatePicker from "react-native-datepicker";
import MapView from 'react-native-maps';
import CheckBox from "react-native-check-box";
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
class FindWashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pickup: { lat: 'null', lng: 'null' },
      showDropoff: false,
      showPickup: true,
      showForm: false,
      isCheckedExperience: false,
      dropoff: { lat: 'null', lng: 'null' },
    };
  }
  componentDidMount() {
    console.log("add event listner");
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  }
  componentWillUnmount() {
    console.log("Remove event listner");
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  }
  // Input Field Handling
  onchangeRequest = () => { }
  onChangeNoofbags = () => { }
  onchangeDetergent = () => { }
  onchangeHung = () => { }
  onchangeInstruction = () => { }
  onchangeScholarship = () => { }
  onchangePrice = () => { }
  // Back Button Handle
  handleBackPress = () => {

    if (this.state.showPickup) {
      this.pickupbackbutton()
      return false;
    }
    else if (this.state.showDropoff) {
      this.dropoffbackbutton()
      return true;

    }
    else if (this.state.showForm) {
      this.formbackbutton();
      return true;

    }
  };
  pickupbackbutton = () => {
    // this.props.navigation.navigate.pop();
    // console.log(this.props);
  };
  dropoffbackbutton = () => {
    this.setState({ pickup: { lat: '', lng: '' }, showPickup: true, showDropoff: false });

  };
  formbackbutton = () => {
    this.setState({ dropoff: { lat: '', lng: '' }, showDropoff: true, showForm: false });
  };


  // Component Handling

  renderInputfield = (headerText, placeholder, ErrTxt, Iserr, onChangeText, image, ) => {

    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <Image
            source={image}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginTop: Metrics.ratio(6)
            }}
          />
          {/* <Icon style={{}} size={25} color="#0f5997" name={"user"} /> */}
          <TextInput
            style={styles.inputField}
            placeholderTextColor="#b4b4b4"
            // secureTextEntry={rightIcon ? this.state.showpassword : false}
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
        </View>
        {Iserr && <View><Text style={{ color: 'red' }} >**{ErrTxt}</Text></View>}
      </View>
    );
  };
  // text Area
  renderInputArea = (headerText, placeholder, ErrTxt, Iserr, onChangeText, image, ) => {

    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputareaHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderWidth: StyleSheet.hairlineWidth,
            borderColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >

          <TextInput
            style={styles.inputareaField}
            numberOfLines={4}
            placeholderTextColor="#b4b4b4"
            // secureTextEntry={rightIcon ? this.state.showpassword : false}
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
        </View>
        {Iserr && <View><Text style={{ color: 'red' }} >**{ErrTxt}</Text></View>}
      </View>
    );
  };

  // Radio Button Component

  renderRadio = (headerText, placeholder1, placeholder2, ErrTxt, Iserr, tooltip, onChangeRadio) => {
    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            // borderBottomWidth: StyleSheet.hairlineWidth,
            // borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <RadioGroup
            color="#ff7ee7"
            style={styles.radioGroup}
            onSelect={(index, value) => {
              onChangeRadio(value)
            }}
          >
            <RadioButton value={placeholder1} color="#b4b4b4" style={styles.radioOptions} >
              <Text>{placeholder1}</Text>
            </RadioButton>


            <RadioButton value={placeholder2} style={styles.radioOptions}>
              <Text>{placeholder2}</Text>
            </RadioButton>
          </RadioGroup>
        </View>
        {Iserr && <View><Text style={{ color: 'red' }} >**{ErrTxt}</Text></View>}
      </View>
    );
  };
  // Checkbox Component
  rendercheckBox = (headerText, placeholder1, placeholder2, ErrTxt, Iserr, onChangeCheck) => {
    return (
      <View style={{
        width: Metrics.screenWidth * 0.9,
        marginHorizontal: Metrics.screenWidth * 0.025,
        marginBottom: Metrics.ratio(10)

      }}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View style={{ flexDirection: "row" }}>
          <CheckBox
            style={{
              width: Metrics.ratio(25),
              height: Metrics.ratio(25),
              borderRadius: 10,
              borderWidth: Metrics.ratio(1),
              justifyContent: "center",
              alignItems: "center",
              borderColor: "#0f5997",
              marginTop: Metrics.ratio(5),
              marginHorizontal: Metrics.ratio(10),

            }}
            checkedImage={
              <Image
                style={{ width: Metrics.ratio(14), height: Metrics.ratio(14) }}
                source={Images.tickIcon}
              />
            }
            uncheckedCheckBoxColor="transparent"
            checkedCheckBoxColor="transparent"
            onClick={() => {
              this.setState({
                isCheckedExperience: !this.state.isCheckedExperience
              });
            }}
            isChecked={this.state.isCheckedExperience}
          /><Text style={{ marginTop: Metrics.ratio(10) }}>{placeholder1}</Text>
        </View>
      </View>

    )
  }

  // Map Handling Functions
  Handlepickup = () => {
    this.setState({ pickup: { lat: '24.8681357', lng: '67.0442549' }, showPickup: false, showDropoff: true });
  }
  handleDropOff = () => {
    this.setState({ dropoff: { lat: '24.8681357', lng: '67.0442549' }, showDropoff: false, showForm: true });
  }


  // Components Of Findwash 
  renderPickup = () => {
    return (
      <View
        style={{
          width: Metrics.screenWidth * 0.95,
          height: Metrics.screenHeight * 0.80,
          marginLeft: Metrics.screenWidth * 0.025,
          borderRadius: Metrics.ratio(10),
          marginTop: Metrics.ratio(10),
          marginBottom: Metrics.ratio(10),
          paddingVertical: Metrics.ratio(20),
          backgroundColor: "white",
          elevation: 8
        }}
      >
        <View>
          <Text>Map Integreate Soon</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.Handlepickup();
          }}
          style={styles.submitButtonView}
        >
          {/* <Image
        source={Images.submitButtonIcon}
        style={{
          width: Metrics.ratio(20),
          height: Metrics.ratio(20),
          marginRight: Metrics.ratio(5),
          marginTop: Metrics.ratio(3)
        }}
      /> */}
          <Text
            style={{
              color: "black",
              fontSize: Metrics.ratio(16),
              fontFamily: Fonts.type.demibold
            }}
          >
            Confirm Pickup
      </Text>
        </TouchableOpacity>
      </View>

    )
  }
  renderDropOff = () => {
    return (
      <View
        style={{
          width: Metrics.screenWidth * 0.95,
          height: Metrics.screenHeight * 0.80,
          marginLeft: Metrics.screenWidth * 0.025,
          borderRadius: Metrics.ratio(10),
          marginTop: Metrics.ratio(10),
          marginBottom: Metrics.ratio(10),
          paddingVertical: Metrics.ratio(20),
          backgroundColor: "white",
          elevation: 8
        }}
      >
        <View>
          <Text>Map Integreate Soon</Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            this.handleDropOff();
          }}
          style={styles.submitButtonView}
        >
          {/* <Image
        source={Images.submitButtonIcon}
        style={{
          width: Metrics.ratio(20),
          height: Metrics.ratio(20),
          marginRight: Metrics.ratio(5),
          marginTop: Metrics.ratio(3)
        }}
      /> */}
          <Text
            style={{
              color: "black",
              fontSize: Metrics.ratio(16),
              fontFamily: Fonts.type.demibold
            }}
          >
            Confirm DropOff
      </Text>
        </TouchableOpacity>
      </View>

    )
  }
  renderForm = () => {
    return (
      <ScrollView keyboardShouldPersistTaps="always">
        <View
          style={{
            width: Metrics.screenWidth * 0.95,
            marginLeft: Metrics.screenWidth * 0.025,
            borderRadius: Metrics.ratio(10),
            marginTop: Metrics.ratio(10),
            marginBottom: Metrics.ratio(30),
            paddingVertical: Metrics.ratio(20),
            backgroundColor: "white",
            elevation: 8
          }}
        >
          {/* <View style={{marginBottom:Metrics.ratio(10}}> */}
          {
            this.renderInputfield(
              "NO OF BAGS",
              "No of bags",
              "Required no of bags",
              false,
              this.onChangeNoofbags,
              Images.passwordIcon,

            )}
          {
            this.rendercheckBox("Request Same Gender (Optional)", "Yes Please", "", "", this.onchangeRequest())
          }
          {
            this.renderRadio(
              "USING YOUR OWN DETERGENT",
              "Yes",
              "No",
              "Select Field",
              false,
              false,
              this.onchangeDetergent
            )
          }
          {
            this.renderRadio(
              "FOLDED",
              "Yes",
              "No",
              "Select Field",
              false,
              false,
              this.onchangeDetergent
            )
          }
          {
            this.renderRadio(
              "HUNG",
              "Yes",
              "No",
              "Select Field",
              false,
              false,
              this.onchangeHung
            )
          }
          {
            this.renderInputArea(
              "SPECIAL INSTRUCTIONS",
              "What do tou need to Hung up",
              "Required no of bags",
              false,
              this.onchangeInstruction,

            )
          }
          {
            this.renderInputfield(
              "SCHOLARSHIP DONATION",
              "$1",
              "Required field",
              false,
              this.onchangeScholarship,
              Images.Donate

            )
          }
          {
            this.renderInputfield(
              "TOTAL PRICE",
              "total price",
              "Required field",
              false,
              this.onchangePrice,
              Images.Coin

            )
          }
          {/* </View> */}
          <TouchableOpacity
            onPress={() => {
              // this.handleLogin();
            }}
            style={styles.submitButton}
          >
           
            <Text
              style={{
                color: "black",
                fontSize: Metrics.ratio(16),
                fontFamily: Fonts.type.demibold
              }}
            >
             SUBMIT
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
  // Components Of Findwash 


  render() {
    return (
      <View style={styles.container}>
        <Header
          headerText={"FIND A WASH"}
          leftIcon={Images.LeftArrow}
          leftBtnPress={() => {
            if (this.state.showPickup) {
              this.pickupbackbutton()
            }
            else if (this.state.showDropoff) {
              this.dropoffbackbutton()
            }
            else if (this.state.showForm) {
              this.formbackbutton();
            }
          }}

        />
        {this.state.showPickup && this.renderPickup()}
        {this.state.showDropoff && this.renderDropOff()}
        {this.state.showForm && this.renderForm()}
      </View>

    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(FindWashScreen);
