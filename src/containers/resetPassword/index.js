// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  StyleSheet,
  ImageBackground,
} from "react-native";
import styles from "./styles";
import { Metrics, Images, Fonts } from "../../theme";
import Icon from "react-native-vector-icons/FontAwesome";

class PasswordresetScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      password: null
    };
  }

  onChangeEmail = text => {
    this.setState({ email: text });
  };

 

  handleResetpassword = () => {
    const { email, password } = this.state;
    console.log(email, "eeeeeeeeeeeeeeeeeeeeeeee");
    
  };

  renderInputfield = (headerText, placeholder, onChangeText, image) => {
    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <Image
            source={image}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginTop: Metrics.ratio(6)
            }}
          />
          {/* <Icon style={{}} size={25} color="#0f5997" name={"user"} /> */}
          <TextInput
            style={styles.inputField}
            placeholderTextColor="#b4b4b4"
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
        </View>
      </View>
    );
  };

  render() {
    return (
      <ImageBackground
      source={Images.loginBackground}
      // resizeMode = 'contain'
      resizeMethod='auto'
      style={{ width: '100%', height: '100%' }}
    >
      <View style={styles.container}>
        {/* <View style={styles.headerView}>
          <Image
            source={Images.LoginHeaderIcon}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginRight: Metrics.ratio(5)
            }}
          />
          <Text style={styles.headerText}>LOGIN</Text>
        </View> */}
        <View style={styles.bodyView}>
        <View style={styles.logoheaderView}>
              <Image
                source={Images.logo}
                style={{
                  resizeMode: 'center',
                  resizeMethod: 'auto',
                  width: Metrics.ratio(200),
                  height: Metrics.ratio(200),
                  marginBottom: Metrics.ratio(15)
                }}
              />
            </View>
            <View style={styles.headerView}>

              <Text style={styles.headerText}>RESET PASSWORD</Text>
            </View>
          {this.renderInputfield(
            "EMAIL",
            "Student Email .(edu)",
            this.onChangeEmail,
            Images.emailIcon
          )}
         
          <TouchableOpacity
            onPress={() => {
              this.handlehandleResetpassword();
            }}
            style={styles.submitButtonView}
          >
           
            <Text
              style={{
                color: "black",
                fontSize: Metrics.ratio(16),
                fontFamily: Fonts.type.demibold
              }}
            >
              RESET PASSWORD
            </Text>
          </TouchableOpacity>
          <View
            style={{
              flexDirection: "row",
              width: Metrics.screenWidth * 0.9,
              justifyContent: "center",
              marginTop: Metrics.ratio(20)
            }}
          >
            <Text
              style={{
                fontSize: Metrics.ratio(13),
                color: "black",
                fontFamily: Fonts.type.demibold
              }}
            >
              DON'T HAVE ACCOUNT ?{" "}
            </Text>
            <TouchableOpacity
              onPress={() => {

                this.props.navigation.navigate("registrationScreen", {
                  screen: "registrationScreen"
                });
                
              }}
            >
              <Text
                style={{
                  fontSize: Metrics.ratio(13),
                  color: "#0f5997",
                  fontFamily: Fonts.type.demibold
                }}
              >
                REGISTER{" "}
              </Text>
            </TouchableOpacity>
          </View>
       
          <View
            style={{
              flexDirection: "row",
              width: Metrics.screenWidth * 0.9,
              justifyContent: "center",
              marginTop: Metrics.ratio(10)
            }}
          >
            <Text
              style={{
                fontSize: Metrics.ratio(13),
                color: "black",
                fontFamily: Fonts.type.demibold
              }}
            >
              ALREADY HAVE AN ACCOUNT ?{" "}
            </Text>
            <TouchableOpacity
              onPress={() => {

                this.props.navigation.pop()
                
              }}
            >
              <Text
                style={{
                  fontSize: Metrics.ratio(13),
                  color: "#0f5997",
                  fontFamily: Fonts.type.demibold
                }}
              >
                SIGN IN{" "}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = () => ({});

const actions = {};

export default connect(
  mapStateToProps,
  actions
)(PasswordresetScreen);
