// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  Text,
  View,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  ImageBackground,
  Platform
} from "react-native";
import styles from "./styles";
import { Metrics, Images, Fonts } from "../../theme";
import Icon from "react-native-vector-icons/FontAwesome";
import { request as login_request } from '../../actions/Login';
import firebase from 'react-native-firebase';
class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      showpassword: true,
      device_token:'',
      device_type:'',
      validationError: { emailErr: false, passErr: false },
      error: { emailErr: false, passErr: false },
    };
  }
  componentDidMount() {
    this.setState({device_type:Platform.OS});
    this.getpermission();
    // this.props.navigation.navigate('dashboard');
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.login) {
      console.log(nextProps.login, "nextProps.loginnextProps.login");
      if (
        !nextProps.login.failure &&
        !nextProps.login.isFetching &&
        nextProps.login.data &&
        nextProps.login.data.data.user.access_token
      ) {
        console.log("hello")
        this.props.navigation.navigate("dashboard");
      }
    }
  }

  getpermission = ()=>{
    firebase.messaging().hasPermission()
  .then(enabled => {
    if (enabled) {
     this.gettoken();
      // user has permissions
    } else {
   
      // user doesn't have permission
    } 
  });
  }
  gettoken = async ()=>{
    const fcmToken = await firebase.messaging().getToken();
    
    if (fcmToken) {
      this.setState({device_token:fcmToken});
      // console.log(this.state)
      
      // user has a device token
  } else {
      // user doesn't have a device token yet
  }
  };

  onChangeEmail = text => {
    this.setState({ email: text });
  };

  onChangePassword = text => {
    this.setState({ password: text });
  };

  handleLogin = () => {
    const { email, password,device_token,device_type } = this.state;
    var emailExtension;

    if (!email.match(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/) || email == "" || email == " ") {
      this.setState({ validationError: { emailErr: true, passErr: false } });
      setTimeout(() => {
        this.setState({ validationError: { emailErr: false, passErr: false } });
      }, 3000);
    }

    else if (password == "" || password == " " || password.length < 6) {
      this.setState({ validationError: { emailErr: false, passErr: true } });
      setTimeout(() => {
        this.setState({ validationError: { emailErr: false, passErr: false } });
      }, 3000);
    }
    else {
      var emaila = email.indexOf('@');
      // var emaildot = email.indexOf('.');
      // console.log(emaila, emaildot);
      // if (emaildot > emaila) {
      //   emailExtension = email.slice(emaila + 1, emaildot);
      //   console.log(emaila, emaildot, emailExtension);
      // }
      // else if (emailExtension) {

        let payload = { email, password ,device_token,device_type};
        this.props.login_request(payload);
        // this.props.navigation.navigate('dashboard');
        // console.log(email, "eeeeeeeeeeeeeeeeeeeeeeee");
        // console.log(password, "passwordpasswordpassword");

      // }
    }
  };

  onClickEyeIcon = () => {
    if (this.state.showpassword === true) {
      this.setState({ showpassword: false });
    }

    else if (this.state.showpassword === false) {
      this.setState({ showpassword: true });
    }

  };

  renderInputfield = (headerText, placeholder, ErrTxt, Iserr, onChangeText, image, rightIcon, onRightIconClick) => {

    return (
      <View style={styles.inputFieldView}>
        <Text style={styles.inputFieldHeaderText}>{headerText}</Text>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
            paddingLeft: Metrics.ratio(5),
            width: Metrics.screenWidth * 0.9,
            borderBottomWidth: StyleSheet.hairlineWidth,
            borderBottomColor: "#b4b4b4",
            marginBottom: Metrics.ratio(10)
          }}
        >
          <Image
            source={image}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginTop: Metrics.ratio(6)
            }}
          />
          {/* <Icon style={{}} size={25} color="#0f5997" name={"user"} /> */}
          <TextInput
            style={styles.inputField}
            placeholderTextColor="#b4b4b4"
            secureTextEntry={rightIcon ? this.state.showpassword : false}
            placeholder={placeholder}
            onChangeText={text => {
              onChangeText(text);
            }}
          />
          {rightIcon && <TouchableOpacity style={{ position: "absolute", right: Metrics.ratio(0) }} onPress={() => onRightIconClick()}>
            <Image source={this.state.showpassword ? Images.view : Images.hide} />
          </TouchableOpacity>}
        </View>
        {Iserr && <View><Text style={{ color: 'red' }} >**{ErrTxt}</Text></View>}

      </View>
    );
  };

  render() {
    return (
      <ImageBackground
        source={Images.loginBackground}
        // resizeMode = 'contain'
        resizeMethod='auto'
        style={{ width: '100%', height: '100%' }}
      >
        <View style={styles.container}>
          {/* <View style={styles.headerView}>
          <Image
            source={Images.LoginHeaderIcon}
            style={{
              width: Metrics.ratio(20),
              height: Metrics.ratio(20),
              marginRight: Metrics.ratio(5)
            }}
          />
          <Text style={styles.headerText}>LOGIN</Text>
        </View> */}
          <ScrollView keyboardShouldPersistTaps="always">
            <View style={styles.bodyView}>
              <View style={styles.logoheaderView}>
                <Image
                  source={Images.logo}
                  style={{
                    resizeMode: 'center',
                    resizeMethod: 'auto',
                    width: Metrics.ratio(200),
                    height: Metrics.ratio(200),
                    marginBottom: Metrics.ratio(15)
                  }}
                />
              </View>
              <View style={styles.headerView}>

                <Text style={styles.headerText}>SIGN IN</Text>
              </View>
              {this.renderInputfield(
                "EMAIL",
                "Student Email .(edu)",
                "Email Required must be someone@example.edu",
                this.state.validationError.emailErr,
                this.onChangeEmail,
                Images.emailIcon
              )}
              {
                this.renderInputfield(
                  "PASSWORD",
                  "Enter Password",
                  "Password Required length Should be more than 5",
                  this.state.validationError.passErr,
                  this.onChangePassword,
                  Images.passwordIcon,
                  Images.passwordIcon,
                  this.onClickEyeIcon
                )}
              <TouchableOpacity
                onPress={() => {
                  this.handleLogin();
                }}
                style={styles.submitButtonView}
              >
                {/* <Image
              source={Images.submitButtonIcon}
              style={{
                width: Metrics.ratio(20),
                height: Metrics.ratio(20),
                marginRight: Metrics.ratio(5),
                marginTop: Metrics.ratio(3)
              }}
            /> */}
                <Text
                  style={{
                    color: "black",
                    fontSize: Metrics.ratio(16),
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  LOGIN
            </Text>
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: "row",
                  width: Metrics.screenWidth * 0.9,
                  justifyContent: "center",
                  marginTop: Metrics.ratio(10)
                }}
              >
                <Text
                  style={{
                    fontSize: Metrics.ratio(13),
                    color: "black",
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  FORGET PASSWORD ?{" "}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("passwordScreen", {
                      screen: "passwordScreen"
                    });
                  }}
                >
                  <Text
                    style={{
                      fontSize: Metrics.ratio(13),
                      color: "#0f5997",
                      fontFamily: Fonts.type.demibold
                    }}
                  >
                    RESET{" "}
                  </Text>
                </TouchableOpacity>
              </View>

              <View
                style={{
                  flexDirection: "row",
                  width: Metrics.screenWidth * 0.9,
                  justifyContent: "center",
                  marginTop: Metrics.ratio(10)
                }}
              >
                <Text
                  style={{
                    fontSize: Metrics.ratio(13),
                    color: "black",
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  DON'T HAVE ACCOUNT ?{" "}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("registrationScreen", {
                      screen: "registrationScreen"
                    });
                  }}
                >

                  <Text
                    style={{
                      fontSize: Metrics.ratio(13),
                      color: "#0f5997",
                      fontFamily: Fonts.type.demibold
                    }}
                  >
                    REGISTER AS USER
                  </Text>
                </TouchableOpacity>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  width: Metrics.screenWidth * 0.9,
                  justifyContent: "center",
                  marginTop: Metrics.ratio(10)
                }}
              >
                <Text
                  style={{
                    fontSize: Metrics.ratio(13),
                    color: "black",
                    fontFamily: Fonts.type.demibold
                  }}
                >
                  DON'T HAVE ACCOUNT ?{" "}
                </Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("registrationScreen", {
                      screen: "registrationScreen"
                    });
                  }}
                >

                  <Text
                    style={{
                      fontSize: Metrics.ratio(13),
                      color: "#0f5997",
                      fontFamily: Fonts.type.demibold
                    }}
                  >
                    REGISTER AS WASHER
                  </Text>
                </TouchableOpacity>
              </View>

            </View>
          </ScrollView>
        </View>
      </ImageBackground>
    );
  }
}

const mapStateToProps = (state) => ({
  login: state.login
});

const actions = { login_request };

export default connect(
  mapStateToProps,
  actions
)(LoginScreen);
