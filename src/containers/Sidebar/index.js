// @flow
import { connect } from "react-redux";
import React, { Component } from "react";
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
  AsyncStorage
} from "react-native";
import Actions from "react-native-router-flux";
import { Metrics, Colors, Images } from "../../theme";
import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./styles";


class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isAccountExpand: false,
     
    };
  }


 
  renderBody = () => {
    return (
      <View style={{ marginTop: Metrics.ratio(30), flex: 1 }}>
      
      </View>
    );
  };

  render() {
    const { appConfig } = this.props;

    return (
      <View style={styles.container}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.rowContainer}
        >
          
          {this.renderBody()}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({ });

const actions = { };

export default connect(
  mapStateToProps,
  actions
)(Sidebar);
