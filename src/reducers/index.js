import login from "./Login";
import register from "./register";
import { combineReducers } from "redux";
export const rootReducer = combineReducers({
  login,
  register
});
